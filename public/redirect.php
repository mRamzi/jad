<?php
session_start();
include "./config/db_connect.php";



if (isset($_SESSION['role'])) {

    try {

        $result = $db->query('SELECT COUNT(*) as COUNT from Utilisateurs ;');
        if (!$result) {
            die('<p>ERREUR Requête invalide : ' . $mysqli->error . '</p>');
        }
        $resultCount = $result->num_rows;

        for ($i = 0; $i < $resultCount; $i++) {
            $row = $result->fetch_assoc();
            $countUsers = $row['COUNT'];

        }
        $_SESSION["userCount"] = $countUsers;

        $result->free();

    } catch (Exception $e) {

        die('Erreur : ' . $e->getMessage());

    }

    try {

        $result = $db->query('SELECT COUNT(*) as armoiresCount from Armoires ;');
        if (!$result) {
            die('<p>ERREUR Requête invalide : ' . $mysqli->error . '</p>');
        }
        $resultCount = $result->num_rows;

        for ($i = 0; $i < $resultCount; $i++) {
            $row = $result->fetch_assoc();
            $countArmoires = $row['armoiresCount'];

        }
        $_SESSION["ArmoiresCount"] = $countArmoires;

        $result->free();

    } catch (Exception $e) {

        die('Erreur : ' . $e->getMessage());

    }

    try {

        $result = $db->query('SELECT COUNT(*) as outilsCount from Outils ;');
        if (!$result) {
            die('<p>ERREUR Requête invalide : ' . $mysqli->error . '</p>');
        }
        $resultCount = $result->num_rows;

        for ($i = 0; $i < $resultCount; $i++) {
            $row = $result->fetch_assoc();
            $countOutils = $row['outilsCount'];

        }
        $_SESSION["OutilsCount"] = $countOutils;

        $result->free();

    } catch (Exception $e) {

        die('Erreur : ' . $e->getMessage());

    }

    if ($_SESSION['role'] == 1) {

        header("location: ./admin/panel.php");
 

    } else if ($_SESSION['role'] == 2 || $_SESSION['role'] == 3) {

        header("location: ./users/panel.php");

    } 


}

?>
