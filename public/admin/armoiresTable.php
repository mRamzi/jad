<?php

if (isset($_SESSION['role']) && $_SESSION['role'] == 1 ||$_SESSION['role'] == 2 || $_SESSION['role'] == 3){

include "../config/db_connect.php";
?>

<head>
<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"/>
    <script src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="../css/styles.css"/>
    <script type="text/javascript" src="../script/date_time.js"></script>
		<script src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>

    <script
			src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
		<link rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

<script type="text/javascript">

$(document).ready(function() {

    $('.confirmation').on('click', function () {
        return confirm('Confirmer suppression armoire');
    });
});


</script>

</head>

    <div class="table-responsive table-dark col-md-9 container-fluid" id="usersTable">
    <h4> Tableau des Armoires : </h4>  

						<table class="table table-bordered table-hover text-center"
							data-toggle="" data-search="false" data-filter-control="true"
							data-show-export="true" data-click-to-select="true">
							<thead>
								<tr class="">
									<th data-field=id data-filter-control="select"
										data-sortable="true">Id</th>
									<th data-field=nom data-filter-control="select"
                                        data-sortable="true">Nom</th>
                                        <th data-field=outils data-filter-control="select"
										data-sortable="true">Nb. Outils</th>
									


								</tr>

							</thead>

							<tbody>
<?php
$listOutils = array();

try {

    $result = $db->query('SELECT DISTINCT Armoires.id as ARM, Armoires.nom, (SELECT COUNT(*) FROM Outils WHERE Outils.id_armoire = ARM ) as compte from Armoires LEFT JOIN Outils ON Armoires.id=Outils.id_armoire ;');
    if (!$result) {
        die('<p>ERREUR Requête invalide : ' . $mysqli->error . '</p>');
    }
    $resultCount = $result->num_rows;

    for ($i = 0; $i < $resultCount; $i++) {
        $row = $result->fetch_assoc();
        $id = $row['ARM'];
        $nom = $row['nom'];
        $outil = $row['compte'];

        echo '<tr>'; // on crée une nouvelle ligne pour le record.

        echo '<td>' . $id . '</td>' . "\r\n";
        echo '<td><a href="outil.php?id='.$id.'&nom='.$nom.'">' . $nom . '</td></a>' . "\r\n";
        echo '<td>' . $outil . '</td>' . "\r\n";
        
        if ($_SESSION['role'] == 1 || $_SESSION['role'] == 3){

        
        echo '<td><a href="deleteArmoire.php?id='.$id.'" class="confirmation""><button type="button"> Supprimer </button></a>' . "\r\n";
    }
        echo '</tr>';

    }
    $_SESSION["resultCount"] = $resultCount;

    $result->free();

    $db->close();

} catch (Exception $e) {

    die('Erreur : ' . $e->getMessage());

}

?>
										<tr>

										</tr>
							</tbody>







						</table>
					</div>





</div>

<?php
}else {
    header("location: ../index.php");

}
?>