<?php
session_start();
if (isset($_SESSION['role']) && $_SESSION['role'] == 1) {

    ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html
	xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"/>
		<link rel="stylesheet" type="text/css" media="screen" href="../css/styles.css"/>
		<script type="text/javascript" src="../script/date_time.js"></script>
		<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<script type="text/javascript">
    $(document).ready(function() {
		$('.addUser-div').click(function() {
			$('.modal').modal('show')
		})

		$('.confirmation').on('click', function () {
        return confirm('Confirmer suppression utilisateur');
    });

	});
	</script>
		</head>
		<body>
			<title>Espace de Connexion</title>
			<nav class="navbar navbar-expand-lg bg-dark navbar-dark static-top">
				<div class="container-fluid" >
					<a class="navbar-brand" href="#">
						<img src="../images/Logoestia.png" class="img-responsive" width="150" alt="">
						</a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarResponsive">
							<ul class="navbar-nav ml-auto">
								<li class="nav-item ">
									<a class="nav-link" href="./panel.php">Tableau De Bord
							
										<span class="sr-only">(current)</span>
									</a>
								</li>
								<li class="nav-item ">
									<a class="nav-link" href="./armoires.php">Gestion des Armoires
							
										<span class="sr-only">(current)</span>
									</a>
								</li>
								<li class="nav-item ">
									<a class="nav-link" href="./outils.php">Gestion des Outils
							
										<span class="sr-only">(current)</span>
									</a>
								</li>
								<li class="nav-item active">
									<a class="nav-link" href="./users.php">Gestion Employ&eacute;s
							
										<span class="sr-only">(current)</span>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="../logout.php">D&eacute;connexion
							
										<span class="sr-only">(current)</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</nav>
				<h2 class="display-4">Gestion utilisateurs</h2>
				<!-- Our Modal !-->
				<div class="modal" id="addUser">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">Ajouter un Utilisateur</h4>
							</button>
						</div>
						<div class="modal-body">
							<div style="display: flex; width: 100%;">
								<form action="./createUser.php">
									<div style="width: 100%;">
										<p>Nom</p>
										<input type="text" name="n" placeholder="Nom"
								required="required" />
									</div>
									<div style="width: 100%;">
										<p>Login</p>
										<input type="text" name="u" placeholder="Identifiant"
								required="required" />
									</div>
									<div style="width: 100%;">
										<p>Mot de passe</p>
										<input type="password" name="p" placeholder="Mot de Passe"
								required="required" />
									</div>

									<div style="width: 100%;">
										<p>Email</p>
										<input type="text" name="m" placeholder="Adresse E-mail"
								required="required" />
									</div>
									<div style="width: 100%;">
										<p>Role</p>
										<select name="role-select">
											<option value="">Veuillez choisir..</option>
											<option value="1">Administrateur</option>
											<option value="2">Operateur</option>
											<option value="3">Chef</option>
										</select>
									</div>
									<p>
										<input type="submit" class="btn btn-primary btn-block btn-large"
								value="Ajouter">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
										</p>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Our Modal !-->
				<link rel="stylesheet"
		href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
				<div class="container">
					<div class="row">
						<INPUT type=button value="Retour " onClick="history.back();">
							<div class="col-md-6 addUser-div container-fluid" style="cursor: pointer;">
								<div class="card-counter info">
									<i class="fa fa-users"></i>
									<span class="count-numbers">
										<?php
    echo $_SESSION["userCount"];?>
									</span>
									<span class="count-name">Ajouter un Utilisateur</span>
								</div>
							</div>
						
						</div>
					</div>
				</div>
				<?php include 'usersTable.php';?>
			</body>
			<footer>
				<p>M.Ramzi</p>
			</footer>
		</html>
		<?php

} else {
    header("location: ../index.php");

}
?>