<?php
session_start();
if (isset($_SESSION['role'])){

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"/>
    <script src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="../css/styles.css"/>
    <script type="text/javascript" src="../script/date_time.js"></script>
		<script src="../js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">







    <title>Espace de Connexion</title>

    <nav class="navbar navbar-expand-lg bg-light static-top">

        <div class="container-fluid" >
           <a class="navbar-brand" href="#">
                <img src="../images/Logoestia.png" class="img-responsive" width="150" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item "><a class="nav-link" href="">Tableau De Bord
							<span class="sr-only">(current)</span>
					</a></li>
                    <li class="nav-item active"><a class="nav-link" href="./armoires.php">Gestion des Armoires
							<span class="sr-only">(current)</span>
					</a></li>

					<li class="nav-item"><a class="nav-link" href="./outils.php">Gestion des Outils
							<span class="sr-only">(current)</span>
					</a></li>

					<li class="nav-item"><a class="nav-link" href="./users.php">Gestion Employ&eacute;s
							<span class="sr-only">(current)</span>
					</a></li>
                    <li class="nav-item"><a class="nav-link" href="../logout.php">D&eacute;connexion
							<span class="sr-only">(current)</span>
					</a></li>


				</ul>
			</div>
        </div>
    </nav>

</head>
<body>
    <h2 class="display-4">Vue Outil</h2>
    <INPUT type=button value="Retour " onClick="history.back();">
	<link rel="stylesheet"
		href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
	<div class="container">
		


    <?php include 'outilView.php';?>





</body>

<footer>
        <p>M.Ramzi</p>
</footer>
</html>

<?php

}else {
    header("location: ../index.php");

}

?>