<?php


if (isset($_SESSION['role'])){

include "../config/db_connect.php";
?>

<head>

<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"/>
    <script src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="../css/styles.css"/>
    <script type="text/javascript" src="../script/date_time.js"></script>
		<script src="../js/bootstrap.min.js"></script>




</head>

    <div class="table-responsive table-dark col-md-9 container-fluid" id="usersTable">
    <h4> Tableau des Outils : </h4>  

						<table class="table table-bordered table-hover text-center"
							data-toggle="" data-search="false" data-filter-control="true"
							data-show-export="true" data-click-to-select="true">
							<thead>
								<tr class="">
									<th data-field=id data-filter-control="select"
										data-sortable="true">Id</th>
									<th data-field=nom data-filter-control="select"
                                        data-sortable="true">Nom</th>
                                    <th data-field=dernUtil data-filter-control="select"
                                        data-sortable="true">Derniere utilisation</th>
                                    <th data-field=disponible data-filter-control="select"
                                        data-sortable="true">Disponible</th>
                                    <th data-field=utilisateur data-filter-control="select"
                                        data-sortable="true">Utilisateur</th>
                                       
                                    <th data-field=utilisateur data-filter-control="select"
										data-sortable="true">Armoire</th>

                                    <!-- ICI JE RAJOUTE LA COLONNE DANS LE TABLEAU !-->
                                    <th data-field=utilisateur data-filter-control="select"
                                        data-sortable="true">Fabricant</th>

								</tr>

							</thead>

							<tbody>
<?php
$currentUser = $_SESSION['login_user'];
try {

    $result = $db->query('SELECT Outils.id,Outils.nom,Outils.dernUtil,Outils.estUtil,Outils.id_utilisateur,Outils.id_armoire, Utilisateurs.nom as nomUtilisateur, Fabricants.nom as nomFabricant, Armoires.nom as nomArmoire FROM Outils LEFT JOIN Utilisateurs ON Utilisateurs.id = Outils.id_utilisateur JOIN Armoires ON Armoires.id = Outils.id_armoire JOIN Fabricants on Fabricants.id = Outils.id_fabricant;');
    if (!$result) {
        die('<p>ERREUR Requête invalide : ' . $mysqli->error . '</p>');
    }
    $resultCount = $result->num_rows;

    for ($i = 0; $i < $resultCount; $i++) {
        $row = $result->fetch_assoc();
        $id = $row['id'];
        $nom = $row['nom'];
        $dernUtil = $row['dernUtil'];
        $fabricants = $row['nomFabricant'];
        if ($row['estUtil'] == 0) {
            $dispo = "Disponible";

        } else {
            $dispo = "Non Disponible";
        }       
        $utilisateur = $row['nomUtilisateur'];
        $armoire = $row['nomArmoire'];



        echo '<tr>'; // on crée une nouvelle ligne pour le record.

        echo '<td>' . $id . '</td>' . "\r\n";
        echo '<td>' . $nom . '</td></a>' . "\r\n";
        echo '<td>' . $dernUtil . '</td>' . "\r\n";
        echo '<td>' . $dispo . '</td>' . "\r\n";
        echo '<td>' . $utilisateur . '</td>' . "\r\n";
        echo '<td>' . $armoire . '</td>' . "\r\n";
        echo '<td>' . $fabricants . '</td>' . "\r\n";

        echo '<td><a href="../users/utiliseroutil.php?id='.$id.'&user='.$currentUser.'"><button type="button"> Utiliser </button>' . "\r\n";
        echo '<td><a href="../users/deposeroutil.php?id='.$id.'&user='.$currentUser.'"><button type="button"> D&eacute;poser </button>' . "\r\n";

        echo '</tr>';

    }
    $_SESSION["resultCount"] = $resultCount;

    $result->free();

    $db->close();

} catch (Exception $e) {

    die('Erreur : ' . $e->getMessage());

}

?>
										<tr>

										</tr>
							</tbody>







						</table>
					</div>





</div>

<?php
}else {
    header("location: ../index.php");

}
?>