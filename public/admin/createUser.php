<?php
session_start();
include "../config/db_connect.php";

include '../config/functions.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (isset($_SESSION['login_user']) && $_SESSION['role'] == 1) {
    $salt         = getSalt();

    $nomUtilisateur = $_GET['n'];
    $loginUtilisateur = $_GET['u'];
    $motdepasseUtilisateur = $_GET['p'];
    $passwordHash = password_hash(concatPasswordWithSalt($motdepasseUtilisateur,$salt),PASSWORD_DEFAULT);
    $emailUtilisateur = $_GET['m'];
    $roleUtilisateur = $_GET['role-select'];

    $currentUser = $_SESSION['login_user'];

    $stmt = mysqli_prepare($db, 'INSERT INTO Utilisateurs(nom,login,motdepasse,id_role,email,salt) VALUES(?,?,?,?,?,?);');
    $stmt->bind_param("sssiss", $nomUtilisateur, $loginUtilisateur, $passwordHash, $roleUtilisateur, $emailUtilisateur,$salt);
    $stmt->execute();

    if (!$stmt) {
        die('<p>ERREUR Requête invalide : ' . $mysqli->error . '</p>');
    }
    $stmt->close();

    header("location: users.php");


    

}else{
   header("location: ../index.php");

}

