<?php

if (isset($_SESSION['role'])){


include "../config/db_connect.php";

$idOutil = $_GET['id'];
$nomArmoire = $_GET['nom'];

?>
 
 <head>

 <link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"/>
    <script src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="../css/styles.css"/>
    <script type="text/javascript" src="../script/date_time.js"></script>
		<script src="../js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">




</head>

    <div class="table-responsive table-dark col-md-9 container-fluid" id="usersTable">
    <h4> Les Outils dans <?php 
echo $nomArmoire;?> </h4>

						<table class="table table-bordered table-hover text-center"
							data-toggle="" data-search="false" data-filter-control="true"
							data-show-export="true" data-click-to-select="true">
							<thead>
								<tr class="">
									<th data-field=id data-filter-control="select"
										data-sortable="true">Id</th>
									<th data-field=nom data-filter-control="select"
                                        data-sortable="true">Nom</th>
                                        <th data-field=dernUtil data-filter-control="select"
                                        data-sortable="true">Derniere Utilisation</th>
                                        <th data-field=outils data-filter-control="select"
                                        data-sortable="true">Disponibilité</th>
                                        <th data-field=outils data-filter-control="select"
										data-sortable="true">Utilisateur</th>



								</tr>

							</thead>

							<tbody>
<?php

try {

    $stmt = mysqli_prepare($db, 'SELECT Outils.id, Outils.nom, Outils.dernUtil,Outils.estUtil, Outils.id_utilisateur as user, Utilisateurs.nom as nomUser FROM Outils JOIN Armoires ON Outils.id_armoire = Armoires.id LEFT JOIN Utilisateurs ON Outils.id_utilisateur = Utilisateurs.id WHERE Outils.id_armoire = ?');
    $stmt->bind_param("i", $idOutil);
    $stmt->execute();
    $result = $stmt->get_result();
    if (!$result) {
        die('<p>ERREUR Requête invalide : ' . $mysqli->error . '</p>');
    }
    $resultCount = $result->num_rows;

    for ($i = 0; $i < $resultCount; $i++) {
        $row = $result->fetch_assoc();
        $id = $row['id'];
        $nom = $row['nom'];
        $dernUtil = $row['dernUtil'];
        if ($row['estUtil'] == 0) {
            $dispo = "Disponible";

        } else {
            $dispo = "Non Disponible";
        }
        
        $utilisateur = $row['nomUser'];
        $currentUser = $_SESSION['user_id'];
        echo '<tr>'; // on crée une nouvelle ligne pour le record.

        echo '<td>' . $id . '</td>' . "\r\n";
        echo '<td>' . $nom . '</td></a>' . "\r\n";
        echo '<td>' . $dernUtil . '</td>' . "\r\n";
        echo '<td>' . $dispo . '</td>' . "\r\n";
        echo '<td>' . $utilisateur . '</td>' . "\r\n";

        echo '<td><a href="../users/utiliseroutil.php?id='.$id.'&user='.$currentUser.'"><button type="button"> Utiliser </button>' . "\r\n";
        echo '<td><a href="../users/deposeroutil.php?id='.$id.'&user='.$currentUser.'"><button type="button"> D&eacute;poser </button>' . "\r\n";
       
        echo '</tr>';

    }
    $_SESSION["resultCount"] = $resultCount;

    $result->free();

    $db->close();

} catch (Exception $e) {

    die('Erreur : ' . $e->getMessage());

}

?>
										<tr>

                                        </tr>
                                        
							</tbody>







						</table>
					</div>

                    



</div>

<?php

}else {
    header("location: ../index.php");

}

?>