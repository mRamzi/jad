<?php
session_start();
include('../config/db_connect.php');
include('../config/functions.php');


if (isset($_SESSION['role']) && $_SESSION['role'] == 1) {

    $idUtilisateur = $_GET['id'];
    $nouveauMotdepasse = $_GET['password'];
    $salt = $_GET['salt'];


    $newPassword = password_hash(concatPasswordWithSalt($nouveauMotdepasse,$salt),PASSWORD_DEFAULT);

    $currentUser = $_SESSION['login_user'];

    $stmt = mysqli_prepare($db, 'UPDATE Utilisateurs SET motdepasse = ? WHERE id = ?;');
    $stmt->bind_param("si",$newPassword, $idUtilisateur);
    $stmt->execute();
    $stmt->close();


        header('Location: ../admin/users.php');

}else{
    header("location: ../index.php");

}

?>

