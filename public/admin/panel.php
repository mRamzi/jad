<?php
session_start();
include "../config/db_connect.php";

if (isset($_SESSION['role']) && $_SESSION['role'] == 1) {
    ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html
	xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"/>
		<script src="../js/bootstrap.min.js"></script>
		<link rel="stylesheet" type="text/css" media="screen" href="../css/styles.css"/>
		<script type="text/javascript" src="../script/date_time.js"></script>
		<script src="../js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="../fonts/css/all.css">
			<title>Tableau de Bord</title>
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
				<div class="container-fluid" >
					<a class="navbar-brand" href="#">
						<img src="../images/Logoestia.png" class="img-responsive" width="150" alt="">
						</a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarResponsive">
							<ul class="navbar-nav ml-auto">
								<li class="nav-item active">
									<a class="nav-link" href="">Tableau De Bord

										<span class="sr-only">(current)</span>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="./armoires.php">Gestion des Armoires

										<span class="sr-only">(current)</span>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="./outils.php">Gestion des Outils

										<span class="sr-only">(current)</span>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="./users.php">Gestion Employ&eacute;s

										<span class="sr-only">(current)</span>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="../logout.php">D&eacute;connexion

										<span class="sr-only">(current)</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</nav>
			</head>
			<body>
				<h2 class="display-4" id="greetins">Bon retour,

					<?php
    echo $_SESSION['login_user'];

    ?>
				</h2>
				<h2 class="display-4">Tableau de Bord</h2>
				<h3>
					<span id="date_time"></span>
					<script type="text/javascript">
			window.onload = date_time('date_time');
		</script>
				</h3>
				<div class="container">
					<div class="row">
						<div class="col-md-6" style="cursor: pointer;"
				onclick="window.location='./armoires.php';">
							<div class="card-counter primary">
								<i class="fas fa-archive"></i>
								<span class="count-numbers">Gestion des armoires</span>
								<span class="count-name"><?php

    echo $_SESSION['ArmoiresCount'];

    ?> Armoires</span>
							</div>
						</div>
						<div class="col-md-6" style="cursor: pointer;"
				onclick="window.location='./outils.php';">
							<div class="card-counter color">
								<i class="fas fa-tools"></i>
								<span class="count-numbers">Gestion des outils</span>
								<span class="count-name"><?php

    echo $_SESSION['OutilsCount'];

    ?> Outils</span>
							</div>
						</div>
						
						<div class="col-md-12" style="cursor: pointer;"
				onclick="window.location='./users.php';">
							<div class="card-counter info">
								<i class="fa fa-users"></i>
								<span class="count-numbers">Gestion utilisateurs</span>
								<span class="count-name">
									<?php

    echo $_SESSION['userCount'];

    ?> Utilisateurs
								</span>
							</div>
						</div>
					</div>
				</div>
			</body>
			<footer>
				<p>M.Ramzi</p>
			</footer>
			<?php

    

} else {

    header("location: ../users/panel.php");

}

?>