<?php
session_start();

if (isset($_SESSION['role'])) {

    ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
		<link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"/>
		<link rel="stylesheet" type="text/css" media="screen" href="../css/styles.css"/>
		<script type="text/javascript" src="../script/date_time.js"></script>
		<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
		<link rel="stylesheet" href="../fonts/css/all.css">

        
		<script src="../js/bootstrap.min.js"></script>
	<script type="text/javascript">
    $(document).ready(function() {
		$('.addOutil-div').click(function() {
			$('.modal').modal('show')



        $.ajax({
            type:'GET',
            url:'armoiresView.php',
            success:function(data){
                console.log(data);
            }
        });
        });
	});


	</script>






    <title>Outils</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg bg-dark navbar-dark static-top">

        <div class="container-fluid" >
           <a class="navbar-brand" href="#">
                <img src="../images/Logoestia.png" class="img-responsive" width="150" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
                <li class="nav-item "><a class="nav-link" href="./panel.php">Tableau De Bord
							<span class="sr-only">(current)</span>
					</a></li>

					<li class="nav-item "><a class="nav-link" href="./armoires.php">Gestion des Armoires
							<span class="sr-only">(current)</span>
					</a></li>

					<li class="nav-item active"><a class="nav-link" href="./outils.php">Gestion des Outils
							<span class="sr-only">(current)</span>
					</a></li>
					<?php

if ($_SESSION['role'] == 1){

?>
					<li class="nav-item"><a class="nav-link" href="./users.php">Gestion Employ&eacute;s
							<span class="sr-only">(current)</span>
					</a></li>
<?php
}
?>
                    <li class="nav-item"><a class="nav-link" href="../logout.php">D&eacute;connexion
							<span class="sr-only">(current)</span>
					</a></li>

				</ul>
			</div>
        </div>
    </nav>



    <h2 class="display-4">Liste des Outils</h2>
    <!-- Our Modal !-->
		<div class="modal" id="addArmoire">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">Ajouter une armoire</h4>
							</button>
						</div>
						<div class="modal-body">
							<div style="display: flex; width: 100%;">
								<form action="./createArmoire.php">
									<div style="width: 100%;">
										<p>Nom</p>
										<input type="text" name="n" placeholder="Nom"
								required="required" />
									</div>

                                    <div style="width: 100%;">
										<p>Armoire</p>

                                      <select name="armoireSelect" id="displayDetails">
                                       <?php

									   echo '<option value="">Veuillez Choisir...</option>';


                                       ?>



                                      </select>


									</div>
									<p>
										<input type="submit" class="btn btn-primary btn-block btn-large"
								value="Ajouter">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
										</p>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Our Modal !-->
    <INPUT type=button value="Retour " onClick="history.back();">

	<div class="container">
	<?php
      if ($_SESSION['role'] == 1 || $_SESSION['role'] == 3){

	  
	?>
    <div class="row">
							<div class="col-md-6 addOutil-div container-fluid" style="cursor: pointer;">
								<div class="card-counter info">
									<i class="fas fa-archive"></i>
									<span class="count-numbers"></span>
									<span class="count-name">Ajouter un Outil</span>
								</div>
							</div>
							
						</div>
					</div>
					<br>
<?php
	  }
	  ?>

    <?php include 'outilsView.php';?>





</body>

<footer>
        <p>M.Ramzi</p>
</footer>
</html>

<?php

} else {
    header("location: ../index.php");

}

?>