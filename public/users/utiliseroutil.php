<?php
    session_start();

if (isset($_SESSION['role'])) {

    include "../config/db_connect.php";
    $idOutil = $_GET['id'];
    $userid = $_SESSION['user_id'];

    $currentUser = $_SESSION['login_user'];

    $stmt = mysqli_prepare($db, "UPDATE Outils SET id_utilisateur = ?, estUtil = 1, dernUtil = NOW() WHERE id = ?;");
    $stmt->bind_param("ii", $userid, $idOutil);
    $stmt->execute();
    $stmt->close();

    if($_SESSION['role'] == 1){

    header('Location: ../admin/outils.php');

    }else{
        header('Location: ../admin/outils.php');


    }
}else{
    header("location: ../index.php");

}


?>