<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
include "./config/db_connect.php";
include "./config/functions.php";
session_start();

if (isset($_POST['username'])) {
    // username and password sent from form

    $myusername = mysqli_real_escape_string($db, $_POST['username']);
    $mypassword = mysqli_real_escape_string($db, $_POST['password']);
    $sql = mysqli_prepare($db, 'SELECT id,id_role,motdepasse,salt,login FROM Utilisateurs WHERE login = ? ;');
    $sql->bind_param("s", $myusername);
    $sql->execute();
    $result = $sql->get_result();

    if (!$result) {
        die('<p>ERREUR Requête invalide : ' . $mysqli->error . '</p>');
    }
    $resultCount = $result->num_rows;

    $row = $result->fetch_assoc();

    $userid = $row['id'];
    $role = $row['id_role'];
    $salt = $row['salt'];
    $password = $row['motdepasse'];
    $sql->close();
    // If result matched $myusername and $mypassword, table row must be 1 row

    if (password_verify(concatPasswordWithSalt($mypassword,$salt),$password)) {
        $_SESSION['login_user'] = $myusername;
        $_SESSION['user_id'] = $userid;
        $_SESSION['role'] = $role;
      
    
	
	

        header("location: redirect.php");
    } else {
        $error = "Votre nom d'utilisateur ou mot de passe est invalide";

        echo $error;


        header("location: index.php?msg=failed");
    }

			
	
    
    
	
}
