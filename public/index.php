


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <link rel="stylesheet" type="text/css" media="screen" href="./css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"/>
    <script src="./js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="./css/styles.css"/>
    <script type="text/javascript" src="./script/date_time.js"></script>
    <script type="text/javascript" src="./js/jquery-3.3.1.min.js"></script>


    <title>Espace de Connexion</title>

    <nav class="navbar navbar-expand-lg bg-light static-top">

        <div class="container-fluid" >
           <a class="navbar-brand" href="#">
                <img src="./images/Logoestia.png" class="img-responsive" width="150" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active"><a class="nav-link" href="">Connexion
							<span class="sr-only">(current)</span>
					</a></li>



				</ul>
			</div>
        </div>
    </nav>

</head>

<body>



<div class="login ">
              <h3><span id="date_time"></span>
              <script type="text/javascript">window.onload = date_time('date_time');</script></h3>

              <h1>Connexion</h1>
        <form method="post" action="login.php">
            <input type="text" name="username" placeholder="Identifiant" required="required" />
            <input type="password" name="password" placeholder="Mot de Passe" required="required" />
			<button type="submit" class="btn btn-primary btn-block btn-large">Connexion</button>
		</form>
        <br>
    </div>

    <?php
if (isset($_GET["msg"]) && $_GET["msg"] == 'failed') {
    echo '<div class="alert alert-danger container-fluid col-md-9" id="failedAlert" style="text-align:center;" role="alert"> Utilisateur ou Mot De Passe Incorrect ! </div>';
}else if (isset($_GET["msg"]) && $_GET["msg"] == 'loggedout'){
    echo '<div class="alert alert-success container-fluid col-md-9" id="loggedout" style="text-align:center;" role="alert"> D&eacute;connexion R&eacute;ussie, &agrave; bient&ocirc;t.</div>';

}

?>




</body>

<footer>
        <p>M.Ramzi</p>
</footer>
